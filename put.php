<?php
    ini_set('display_errors',1);
    error_reporting(E_ALL);

    $ret = array('status' => 'success', 'message' => '', 'filename' => '');

    $localApiKey = "NgFGsDUKV71XRkcivH1yDkZ1m2HftZwz";
    $remoteApiKey = "";
    if (isset($_GET["apiKey"])) {
        $remoteApiKey = $_GET["apiKey"];
    }

    if ($remoteApiKey != $localApiKey) {
        $ret['status'] = 'failure';
        $ret['message'] = "Access denied!";
    } else {
        if ($_FILES["file"]["error"] > 0) {
            $ret['status'] = 'failure';
            $ret['message'] = "Unable to upload file!";
            #echo json_encode($ret);
        } else {
            $hash = sha1_file($_FILES["file"]["tmp_name"]);
            $lastIndexOf = strrpos($_FILES["file"]["name"], ".");
            $extension = substr($_FILES["file"]["name"], $lastIndexOf);
            $filename = "files/" . $hash . $extension;

            if (file_exists($filename)) {
                $ret['status'] = 'failure';
                $ret['message'] = "File already exists";
                #echo json_encode($ret);
            } else  {
                if (!move_uploaded_file($_FILES["file"]["tmp_name"], $filename)) {
                    $ret['status'] = 'failure';
                    $ret['message'] = "Unable to upload file!";
                    #echo json_encode($ret);         
                } else {
                    $ret['status'] = 'success';
                    $ret['filename'] = "fileServer/" . $filename;
                    #echo json_encode($ret);
                }
            }
        }
    }
    echo json_encode($ret);
?>